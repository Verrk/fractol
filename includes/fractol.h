/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/08 23:26:05 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/01 16:31:39 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <libft.h>
# include <mlx.h>
# include <stdint.h>

# define WIN_X	1280
# define WIN_Y	720

typedef struct	s_nbr
{
	double		r;
	double		i;
}				t_nbr;

typedef struct	s_fract
{
	char		*arg;
	void		*mlx;
	void		*win;
	void		*img;
	char		*data;
	int			bpp;
	int			s_line;
	int			end;
	int			iter;
	double		m_x;
	double		m_y;
	int			zoom;
	double		o_x;
	double		o_y;
	t_nbr		c;
}				t_fract;

#endif
