/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 14:56:01 by cpestour          #+#    #+#             */
/*   Updated: 2014/10/07 08:33:59 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_itoa(int n)
{
	char	*s;
	size_t	i;

	i = ft_nbrlen(n);
	if (n < 0)
	{
		n = -n;
		s = (char *)malloc(ft_nbrlen(n) + 2);
		s[0] = '-';
		i++;
	}
	else
		s = (char *)malloc(ft_nbrlen(n) + 1);
	s[i] = '\0';
	if (n == 0)
		s[0] = '0';
	while (i-- && n)
	{
		s[i] = n % 10 + '0';
		n /= 10;
	}
	return (s);
}
