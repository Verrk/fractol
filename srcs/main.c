/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/08 23:25:36 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/01 00:41:25 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			rgb_to_int(int r, int g, int b)
{
	int		color;

	color = 0;
	color = (r << 16) | (g << 8) | b;
	return (color);
}

int			hsv_to_rgb(int h, int s, int v)
{
	uint8_t	reg;
	uint8_t	f;
	uint8_t	p;
	uint8_t	q;
	uint8_t	t;

	if (s == 0)
		return (rgb_to_int(v, v, v));
	reg = h / 43;
	f = (h - (reg * 43)) * 6;
	p = (v * (255 - s)) >> 8;
	q = (v * (255 - ((s * f) >> 8))) >> 8;
	t = (v * (255 - ((s * (255 - f)) >> 8))) >> 8;
	if (reg == 0)
		return (rgb_to_int(t, p, v));
	else if (reg == 1)
		return (rgb_to_int(v, p, q));
	else if (reg == 2)
		return (rgb_to_int(v, t, p));
	else if (reg == 3)
		return (rgb_to_int(q, v, p));
	else if (reg == 4)
		return (rgb_to_int(p, v, t));
	else
		return (rgb_to_int(p, q, v));
}

void		put_pixel(t_fract *fract, int x, int y, int i)
{
	int		color;
	uint8_t	r;
	uint8_t	g;
	uint8_t	b;

	color = 0;
	color = hsv_to_rgb(i % 256, 255, 255 * (i < fract->iter));
	r = (color & 0x00FF0000) >> 16;
	g = (color & 0x0000FF00) >> 8;
	b = (color & 0x000000FF);
	if (fract->end == 0)
	{
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x)] = b;
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x) + 1] = g;
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x) + 2] = r;
	}
	else
	{
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x) + 1] = r;
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x) + 2] = g;
		fract->data[(y * fract->s_line) + ((fract->bpp / 8) * x) + 3] = b;
	}
}

int			julia(t_fract *fract, int x, int y)
{
	t_nbr	p;
	t_nbr	old;
	t_nbr	new;
	int		i;

	i = 0;
	new.r = 1.5 * (x - WIN_X / 2) / (0.5 * fract->zoom * WIN_X) + fract->o_x;
	new.i = 1.15 * -((y - WIN_Y / 2) / (0.5 * fract->zoom * WIN_Y)) + fract->o_y;
	p.r = fract->c.r;
	p.i = fract->c.i;
	while (i < fract->iter)
	{
		old.r = new.r;
		old.i = new.i;
		new.r = old.r * old.r - old.i * old.i + p.r;
		new.i = 2 * old.r * old.i + p.i;
		if ((new.r * new.r + new.i * new.i) > 4)
			return (i);
		i++;
	}
	return (i);
}

int			mendel(t_fract *fract, int x, int y, int tri)
{
	t_nbr	p;
	t_nbr	old;
	t_nbr	new;
	int		i;

	i = 0;
	new.r = 0;
	new.i = 0;
	p.r = 1.5 * (x - WIN_X / 2) / (0.5 * fract->zoom * WIN_X) + fract->o_x;
	p.i = 1.15 * -((y - WIN_Y / 2) / (0.5 * fract->zoom * WIN_Y)) + fract->o_y;
	while (i < fract->iter)
	{
		old.r = new.r;
		old.i = new.i;
		new.r = old.r * old.r - old.i * old.i + p.r;
		new.i = tri * 2 * old.r * old.i + p.i;
		if ((new.r * new.r + new.i * new.i) > 4)
			return (i);
		i++;
	}
	return (i);
}

void		draw(t_fract *fract)
{
	int		x;
	int		y;

	y = 0;
	while (y < WIN_Y)
	{
		x = 0;
		while (x < WIN_X)
		{
			if (ft_strcmp("mendelbrot", fract->arg) == 0)
				put_pixel(fract, x, y, mendel(fract, x, y, 1));
			else if (ft_strcmp("julia", fract->arg) == 0)
				put_pixel(fract, x, y, julia(fract, x, y));
			else if (ft_strcmp("tricorn", fract->arg) == 0)
				put_pixel(fract, x, y, mendel(fract, x, y, -1));
			x++;
		}
		y++;
	}
}

int			expose_hook(t_fract *fract)
{
	draw(fract);
	mlx_put_image_to_window(fract->mlx, fract->win, fract->img, 0, 0);
	return (0);
}

void		reload(t_fract *fract)
{
	ft_bzero(fract->data, WIN_X * WIN_Y * 4);
	expose_hook(fract);
}

int			key_hook(int keycode, t_fract *fract)
{
	if (keycode == 65307)
	{
		free(fract->arg);
		free(fract->mlx);
		exit(0);
	}
	if (keycode == 65450)
	{
		fract->iter *= 2;
		reload(fract);
	}
	if (keycode == 65455)
	{
		if (fract->iter > 2)
		{
			fract->iter /= 2;
			reload(fract);
		}
	}
	if (keycode == 65362)
	{
		fract->o_y += 0.1 / fract->zoom;
		reload(fract);
	}
	if (keycode == 65364)
	{
		fract->o_y -= 0.1 / fract->zoom;
		reload(fract);
	}
	if (keycode == 65361)
	{
		fract->o_x -= 0.1 / fract->zoom;
		reload(fract);
	}
	if (keycode == 65363)
	{
		fract->o_x += 0.1 / fract->zoom;
		reload(fract);
	}
	return (0);
}

int			mouse_hook(int button, int x, int y, t_fract *fract)
{
	if (button == 1)
	{
		fract->c.r = 1.5 * (x - WIN_X / 2) / (0.5 * fract->zoom * WIN_X) + fract->o_x;
		fract->c.i = 1.15 * -((y - WIN_Y / 2) / (0.5 * fract->zoom * WIN_Y)) + fract->o_y;
		reload(fract);
	}
	if (button == 5)
	{
		if (fract->zoom > 1)
		{
			fract->zoom /= 2;
			reload(fract);
		}
	}
	if (button == 4)
	{
		fract->o_x = 1.5 * (x - WIN_X / 2) / (0.5 * fract->zoom * WIN_X) + fract->o_x;
		fract->o_y = 1.15 * -((y - WIN_Y / 2) / (0.5 * fract->zoom * WIN_Y)) + fract->o_y;
		fract->zoom *= 2;
		reload(fract);
	}
	return (0);
}

int			main(int ac, char **av)
{
	t_fract	fract;

	if (ac == 2)
	{
		fract.arg = ft_strdup(av[1]);
		if (ft_strcmp("mendelbrot", fract.arg) == 0 || ft_strcmp("julia", fract.arg) == 0 || ft_strcmp("tricorn", fract.arg) == 0)
		{
			fract.mlx = mlx_init();
			fract.win = mlx_new_window(fract.mlx, WIN_X, WIN_Y, fract.arg);
			fract.img = mlx_new_image(fract.mlx, WIN_X, WIN_Y);
			fract.data = mlx_get_data_addr(fract.img, &fract.bpp, &fract.s_line, &fract.end);
			fract.iter = 300;
			fract.m_x = -1;
			fract.m_y = 0;
			fract.o_x = 0;
			fract.o_y = 0;
			fract.c.r = 0.285;
			fract.c.i = 0.01;
			fract.zoom = 1;
			mlx_expose_hook(fract.win, expose_hook, &fract);
			mlx_key_hook(fract.win, key_hook, &fract);
			mlx_mouse_hook(fract.win, mouse_hook, &fract);
			mlx_loop(fract.mlx);
		}
	}
	return (0);
}
