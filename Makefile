#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/08 23:19:12 by cpestour          #+#    #+#              #
#    Updated: 2015/12/01 16:27:57 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=fractol
CFLAGS=-Wall -Werror -Wextra -g -I/usr/include -Iincludes -Ilibft/includes
LDFLAGS=-Llibft -lft -L/usr/lib/ -lmlx -lX11 -lXext
SRC=srcs/main.c
OBJ=$(SRC:.c=.o)

all: lib $(NAME)

lib:
	make -C libft

$(NAME): $(OBJ)
	gcc -o $@ $^ $(LDFLAGS)

%.o: %.c
	gcc -o $@ -c $< $(CFLAGS)

clean:
	rm -f *~ $(OBJ) srcs/*~ includes/*~

fclean: clean
	make fclean -C libft
	rm -f $(NAME)

re: fclean all
